# Task-Relevant Encoding of Domain Knowledge in Dynamics Modelling Application to Furnace Forecasting from Video

Simulation code for accepted paper:

    Michael, Brendan, et al. "Task-Relevant Encoding of Domain Knowledge in Dynamics Modelling: Application to Furnace Forecasting from Video." IEEE Access (2022).

Bibtex:

    @article{michael2022task,
    title={Task-Relevant Encoding of Domain Knowledge in Dynamics Modelling: Application to Furnace Forecasting from Video},
    author={Michael, Brendan and Ise, Akifumi and Kawabata, Kaoru and Matsubara, Takamitsu},
    journal={IEEE Access},
    year={2022},
    publisher={IEEE}
    }

Requires dmdtools library
# https://github.com/cwrowley/dmdtools

However, this notebook uses a modified implementation of the above:
# https://gitlab.com/Brendan_Michael/dmdtools

Please install and import as appropriate.
